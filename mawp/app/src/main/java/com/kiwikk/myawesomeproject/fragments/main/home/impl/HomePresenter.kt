package com.kiwikk.myawesomeproject.fragments.main.home.impl

import android.content.SharedPreferences
import android.util.Log
import com.kiwikk.myawesomeproject.di.named.NamedUI
import com.kiwikk.myawesomeproject.fragments.main.home.HomeView
import com.kiwikk.myawesomeproject.fragments.main.home.IHomeInteractor
import com.kiwikk.myawesomeproject.fragments.main.home.IHomePresenter
import com.kiwikk.myawesomeproject.dataclasses.Person
import moxy.InjectViewState
import moxy.MvpPresenter
import rx.Scheduler
import rx.subscriptions.CompositeSubscription
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import javax.inject.Inject

@InjectViewState
class HomePresenter @Inject constructor(val homeInteractor: IHomeInteractor, @NamedUI val obsOnScheduler: Scheduler) : MvpPresenter<HomeView>(), IHomePresenter {
    //TODO: INJECT?
    lateinit var sharedPreferences: SharedPreferences

    lateinit var person: Person
    var id = 1L
    val compositeSubscription = CompositeSubscription()

    init {

    }

    override fun setSharedPrefs(sp: SharedPreferences) {
        sharedPreferences = sp
    }

    override fun firstRun() {
        Log.i("HPresenter", "init")
        compositeSubscription.add(homeInteractor.getPerson()
                .observeOn(obsOnScheduler)
                .subscribe {
                    Log.i("HPresenter", "I'm in subscribe")
                    person = it

                    Log.i("firstRun", "${it.weeks}, ${it.name}")
                    viewState.createTable()
                    if (person.birthDate == LocalDate.now().format(DateTimeFormatter.ofPattern("dd MM yyyy"))) {
                        viewState.setPersonBDate()
                    }
                    viewState.colorWeeks(it.weeks)
                })

    }

    fun insertPerson() {
        val sub = homeInteractor.insertPersonToDB(person)
                .subscribeOn(obsOnScheduler)
                .subscribe(
                        { id = it },
                        { t -> t.printStackTrace() }
                )
        compositeSubscription.add(sub)
    }

//    override fun createCalendar() {
//        val weeks =
//                homeInteractor.getPerson()
//                        .observeOn(obsOnScheduler)
//                        .subscribe {
//                            viewState.createTable()
//                        }
//
//        compositeSubscription.add(weeks)
//    }
//
//    fun colorWeeks() {
//        val weeks =
//                homeInteractor.getPerson()
//                        .observeOn(obsOnScheduler)
//                        .subscribe {
//                            Log.i("HPresenter", "colorweeks $it")
//                            viewState.colorWeeks(it.weeks)
//                        }
//
//        compositeSubscription.add(weeks)
//    }

    override fun setPersonInfo(name: String, birthDay: String, imageUrl:String) {
        val dtf = DateTimeFormatter.ofPattern("dd MM yyyy")
        val birthDateLocal = LocalDate.parse(birthDay, dtf)

        val now = LocalDate.now()
        val weeks = ChronoUnit.WEEKS.between(birthDateLocal, now).toInt()

        person = Person(name = name, birthDate = birthDay, imageUrl = imageUrl, weeks = weeks)

        insertPerson()
        viewState.colorWeeks(person.weeks)
    }


    override fun onDestroy() {
        compositeSubscription.unsubscribe()
    }
}

