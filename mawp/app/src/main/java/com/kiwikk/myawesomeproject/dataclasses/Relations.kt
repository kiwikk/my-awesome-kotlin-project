package com.kiwikk.myawesomeproject.dataclasses

data class Subscription(
        var userName: String = "",
        var subscriptionName: String = ""
)

data class Subscribers(
        var userName: String = "",
        var subscriberName: String = ""
)
