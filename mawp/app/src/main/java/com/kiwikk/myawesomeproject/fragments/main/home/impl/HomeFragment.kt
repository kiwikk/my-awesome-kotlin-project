package com.kiwikk.myawesomeproject.fragments.main.home.impl

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.material.datepicker.*
import com.google.android.material.datepicker.CalendarConstraints.DateValidator
import com.kiwikk.myawesomeproject.R
import com.kiwikk.myawesomeproject.activities.main.MainActivity
import com.kiwikk.myawesomeproject.elements.WeekButton
import com.kiwikk.myawesomeproject.fragments.main.home.HomeView
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


//добавить mvp, интерракторы
//создавать в презентере новый интеррактор
//в интерракторе должны быть данные, которые юзает фрагмент
//нужен репозиторий, репозиторий обращается к базе данных и возвращает данные
//репозиторием пользуется только интеррактор
//сделать поле интеррактора в презенторе и в ините создавать

//презентер ходит в интеррактор за данными
//либо брать контест из вьюхи, либо через презентер


//разбить на интеракторы
//добавить класс андроид апликейшн
//добавить зависимости новых интеракторов, репозиториев и тд, распределено по компонентам должно быть
//юзать их во фрагментах, идти в дагер забирать презентер оттуда
//презентеры, интеракторы и тд должны юзать дагер в конструкторе
//сделать как минимум 2 компонента (апликейшн и активити)

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : MvpAppCompatFragment(), HomeView {
    private lateinit var vview: View
    private lateinit var progressBar: ProgressBar

    @Inject
    @InjectPresenter
    lateinit var homePresenter: HomePresenter


    @ProvidePresenter
    fun provide() = homePresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        (activity as MainActivity).component.inject(this)

        super.onCreate(savedInstanceState)
        homePresenter.setSharedPrefs(requireActivity().getSharedPreferences("com.kiwikk.myawesomeproject", Context.MODE_PRIVATE))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        vview = inflater.inflate(R.layout.fragment_home, container, false)
        progressBar = vview.findViewById(R.id.home_progressbar)

        homePresenter.firstRun()

        return vview
    }

    //обычно юзается грид
    override fun createTable() {
        progressBar.visibility = View.VISIBLE

        activity!!.window.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        val tableLayout = vview.findViewById<TableLayout?>(R.id.tableLayout)
        for (i in 0..WEEK_ROWS) {
            val tableRow = TableRow(this.context)
            for (j in 0..WEEK_COLUMNS) {
                if (i == 0) {
                    val textView = TextView(this.context)
                    if (j != 0 && j % 10 == 0 || j == 1) textView.text = j.toString() else textView.text = ""
                    textView.setPadding(20, 0, 0, 0)
                    tableRow.addView(textView)
                    continue
                }

                val id = (i - 1) * 52 + j
                val weekButton = WeekButton(this.context, id, fragmentManager)

                val p = TableRow.LayoutParams(70, 70)
                p.setMargins(-5, 0, -5, 0)
                weekButton.layoutParams = p

                if (j == 0) {
                    val textView = TextView(this.context)
                    if (i % 10 == 0 || i == 1) textView.text = i.toString() else textView.text = ""
                    val params = TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT)
                    params.setMargins(0, -15, 10, -15)
                    textView.layoutParams = params
                    tableRow.addView(textView)
                    continue
                }
                val params = TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT)
                params.setMargins(0, -10, 25, -10)
                tableRow.layoutParams = params
                tableRow.addView(weekButton, j)
            }
            tableLayout.addView(tableRow, i)
        }

        progressBar.visibility = View.GONE
        activity!!.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    override fun colorWeeks(weeks: Int) {
        Log.i("colorweeks", "$weeks")
        val tableLayout = vview.findViewById<TableLayout?>(R.id.tableLayout)
        for (i in 1..WEEK_ROWS) {
            val tableRow = tableLayout.getChildAt(i) as TableRow
            for (j in 1..WEEK_COLUMNS) {
                val weekButton = tableRow.getChildAt(j) as WeekButton
                if (weekButton.getID() < weeks) weekButton.setLived()
            }
        }
    }

    override fun setPersonBDate() {
        Log.i("Date of birth", "start")

        val dataPicker = MaterialDatePicker.Builder
                .datePicker()
                .setCalendarConstraints(limitRange())
                .setTitleText("Когда у тебя др?")
                .build()

        dataPicker.isCancelable = false
        dataPicker.addOnPositiveButtonClickListener { it ->
            val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
            calendar.timeInMillis = it
            val fm = SimpleDateFormat("dd MM yyyy")
            Log.i("Datepicker date:", fm.format(calendar.time))
            val account = (activity as MainActivity).googleSignInAccount
            homePresenter.setPersonInfo(account?.email
                    ?: "", fm.format(calendar.time), account?.photoUrl.toString())
        }
        dataPicker.show(activity!!.supportFragmentManager, "HomeFragment")
        Log.i("Date of birth", "finish")
    }

    private fun limitRange(): CalendarConstraints {
        val maxDate = Calendar.getInstance()
        val minDate = Calendar.getInstance()
        minDate.add(Calendar.YEAR, -80)
        val dateValidatorMin: DateValidator = DateValidatorPointForward.from(minDate.timeInMillis)
        val dateValidatorMax: DateValidator = DateValidatorPointBackward.before(maxDate.timeInMillis)

        val constraintsBuilder = CalendarConstraints.Builder()

        val listValidators = ArrayList<DateValidator>()
        listValidators.add(dateValidatorMin)
        listValidators.add(dateValidatorMax)
        val validators = CompositeDateValidator.allOf(listValidators)
        constraintsBuilder.setValidator(validators)

        return constraintsBuilder.build()
    }

    companion object {
        private const val WEEK_ROWS = 80 //80
        private const val WEEK_COLUMNS = 52 //52


        fun newInstance(): HomeFragment {
            val fragment = HomeFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}