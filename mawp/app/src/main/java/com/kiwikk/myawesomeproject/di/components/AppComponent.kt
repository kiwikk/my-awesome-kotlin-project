package com.kiwikk.myawesomeproject.di.components

import android.content.Context
import com.kiwikk.myawesomeproject.di.modules.DBRepModule
import com.kiwikk.myawesomeproject.di.named.NamedComputation
import com.kiwikk.myawesomeproject.di.named.NamedUI
import com.kiwikk.myawesomeproject.di.scopes.ApplicationScope
import dagger.BindsInstance
import dagger.Component
import rx.Scheduler

@Component(modules = [DBRepModule::class])
@ApplicationScope
interface AppComponent {
    fun activityComponent(): ActivityComponent

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bindContext(context: Context): Builder

        @BindsInstance
        fun computationScheduler(@NamedComputation scheduler: Scheduler): Builder

        @BindsInstance
        fun UIScheduler(@NamedUI scheduler: Scheduler): Builder
        fun build(): AppComponent
    }
}

