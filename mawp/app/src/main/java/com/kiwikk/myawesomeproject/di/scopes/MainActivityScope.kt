package com.kiwikk.myawesomeproject.di.scopes

import javax.inject.Scope

@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class MainActivityScope