package com.kiwikk.myawesomeproject.fragments

import com.kiwikk.myawesomeproject.callbacks.OnLikeCallback
import com.kiwikk.myawesomeproject.dataclasses.Task

interface Interactor {
    fun updateTask(task: Task)
    fun getUsername():String
    fun setLike(task: Task, onLikeCallback: OnLikeCallback)
}