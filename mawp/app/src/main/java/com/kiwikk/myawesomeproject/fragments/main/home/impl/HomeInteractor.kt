package com.kiwikk.myawesomeproject.fragments.main.home.impl

import android.content.Context
import android.util.Log
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.kiwikk.myawesomeproject.callbacks.OnLikeCallback
import com.kiwikk.myawesomeproject.database.persondb.IDBRepository
import com.kiwikk.myawesomeproject.database.relation.IRelationDbRep
import com.kiwikk.myawesomeproject.database.taskdb.IDBTaskRepository
import com.kiwikk.myawesomeproject.fragments.main.home.IHomeInteractor
import com.kiwikk.myawesomeproject.dataclasses.Person
import com.kiwikk.myawesomeproject.dataclasses.Task
import rx.Observable
import rx.Single
import javax.inject.Inject

class HomeInteractor @Inject constructor(val db: IDBRepository,
                                         val dbTask: IDBTaskRepository,
                                         val context: Context) : IHomeInteractor {

    val account = GoogleSignIn.getLastSignedInAccount(context)?.email!!

    override fun insertPersonToDB(person: Person): Single<Long> {
        return db.insertPerson(person)
    }

    override fun closeDB() {
        db.closeDB()
    }

    override fun getPerson(): Observable<Person> {
        return db.getPerson()
    }

    override fun insertTask(task: Task) {
        Log.i("HInteractor, insert", task.toString())
        dbTask.insertTask(task)
    }

    override fun updateTask(task: Task) {
        dbTask.taskIsDone(task)
    }

    override fun getUsername() = account.split("@")[0]

    override fun setLike(task: Task, onLikeCallback: OnLikeCallback) {
        dbTask.setLikes(task, onLikeCallback)
    }
}