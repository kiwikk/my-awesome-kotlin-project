package com.kiwikk.myawesomeproject.fragments.main.news


import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.kiwikk.myawesomeproject.callbacks.OnLikeCallback
import com.kiwikk.myawesomeproject.database.persondb.IDBRepository
import com.kiwikk.myawesomeproject.database.taskdb.IDBTaskRepository
import com.kiwikk.myawesomeproject.dataclasses.Task
import com.kiwikk.myawesomeproject.fragments.Interactor
import javax.inject.Inject


class NewsInteractor @Inject constructor(val db: IDBRepository,
                                         val dbTask: IDBTaskRepository,
                                         val context: Context) : Interactor {
    val account = GoogleSignIn.getLastSignedInAccount(context)?.email!!


    override fun setLike(task: Task, onLikeCallback: OnLikeCallback) {
        dbTask.setLikes(task, onLikeCallback)
    }

    override fun updateTask(task: Task) {
        dbTask.taskIsDone(task)
    }

    override fun getUsername() = account.split("@")[0]
}