package com.kiwikk.myawesomeproject.adapters

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.ToggleButton
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.google.android.material.chip.Chip
import com.kiwikk.myawesomeproject.R
import com.kiwikk.myawesomeproject.callbacks.OnLikeCallback
import com.kiwikk.myawesomeproject.dataclasses.Task
import com.kiwikk.myawesomeproject.fragments.Interactor
import java.util.*

class TaskRecyclerViewAdapter(val tasks: List<Task>, val interactor: Interactor) : RecyclerView.Adapter<TaskRecyclerViewAdapter.TaskViewHolder>() {
    class TaskViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val header = itemView.findViewById<TextView>(R.id.cardview_header)
        val description = itemView.findViewById<TextView>(R.id.cardview_description)
        val date = itemView.findViewById<TextView>(R.id.cardview_date)
        val author = itemView.findViewById<TextView>(R.id.cardview_author)
        val kicks = itemView.findViewById<TextView>(R.id.cardview_likes)
        val kickBtn = itemView.findViewById<ToggleButton>(R.id.cardview_like)
        val typeChip = itemView.findViewById<Chip>(R.id.task_chip_type)
        val taskCV = itemView.findViewById<MaterialCardView>(R.id.task_cv)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.task_cardview, parent, false)
        return TaskViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.header.text = tasks[position].name
        holder.description.text = tasks[position].description
        holder.date.text = tasks[position].date
        holder.author.text = tasks[position].person_name
        holder.kicks.text = tasks[position].likes.toString()
        holder.typeChip.text = tasks[position].type

        holder.kickBtn.setOnClickListener {
            interactor.setLike(tasks[position], object:OnLikeCallback{
                override fun onLike(likes: Int) {
                    holder.kicks.text = likes.toString()
                }
            })
        }

        holder.typeChip.chipBackgroundColor = if (tasks[position].isPrivate)
            ColorStateList.valueOf(ContextCompat.getColor(holder.itemView.context, R.color.colorPrivate))
        else ColorStateList.valueOf(ContextCompat.getColor(holder.itemView.context, R.color.colorPublic))

        holder.taskCV.setBackgroundColor(if (tasks[position].isDone)
            holder.itemView.context.getColor(R.color.colorTaskDone)
        else holder.itemView.context.getColor(R.color.colorDefault))

        if (interactor.getUsername() == tasks[position].person_name) {
            holder.taskCV.setOnLongClickListener {
                holder.taskCV.setBackgroundColor(holder.itemView.context.getColor(R.color.colorTaskDone))
                tasks[position].isDone = true
                interactor.updateTask(tasks[position])
                return@setOnLongClickListener true
            }
        }
    }

    override fun getItemCount() = tasks.size
}