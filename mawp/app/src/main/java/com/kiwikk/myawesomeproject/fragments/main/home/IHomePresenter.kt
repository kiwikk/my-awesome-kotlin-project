package com.kiwikk.myawesomeproject.fragments.main.home

import android.content.SharedPreferences

interface IHomePresenter {
    fun setSharedPrefs(sp: SharedPreferences)
    fun firstRun()
    fun setPersonInfo(name: String, birthDay: String, imageUrl: String)
}