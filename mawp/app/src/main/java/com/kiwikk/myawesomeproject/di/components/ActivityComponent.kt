package com.kiwikk.myawesomeproject.di.components

import com.kiwikk.myawesomeproject.di.modules.HomeModule
import com.kiwikk.myawesomeproject.di.modules.SearchModule
import com.kiwikk.myawesomeproject.di.modules.SettingsModule
import com.kiwikk.myawesomeproject.di.scopes.MainActivityScope
import com.kiwikk.myawesomeproject.elements.WeekCard
import com.kiwikk.myawesomeproject.fragments.main.home.impl.HomeFragment
import com.kiwikk.myawesomeproject.fragments.main.news.NewsFragment
import com.kiwikk.myawesomeproject.fragments.main.settings.impl.SettingsFragment
import com.kiwikk.myawesomeproject.fragments.subscription.impl.SubscriberFragment
import com.kiwikk.myawesomeproject.fragments.subscription.impl.SubscriptionsFragment
import com.kiwikk.myawesomeproject.fragments.subscription.impl.SearchFragment
import dagger.Subcomponent

@Subcomponent(modules = [HomeModule::class, SettingsModule::class, SearchModule::class])
@MainActivityScope
interface ActivityComponent {
    fun inject(hFragment: HomeFragment)
    fun inject(nFragment: NewsFragment)
    fun inject(sFragment: SettingsFragment)
    fun inject(weekCard: WeekCard)
    fun inject(subscriptionsFragment: SubscriptionsFragment)
    fun inject(subscriberFragment: SubscriberFragment)
    fun inject(searchFragment: SearchFragment)
}