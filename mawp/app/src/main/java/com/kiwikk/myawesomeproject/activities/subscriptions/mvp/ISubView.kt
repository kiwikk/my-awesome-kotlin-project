package com.kiwikk.myawesomeproject.activities.subscriptions.mvp

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface ISubView:MvpView {
    fun beginIonTransaction()
    fun beginErTransaction()
    fun initFragments()
    fun beginSearchTransaction()
}