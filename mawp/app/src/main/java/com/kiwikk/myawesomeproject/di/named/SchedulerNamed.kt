package com.kiwikk.myawesomeproject.di.named

import javax.inject.Qualifier

@Qualifier
@Retention(value = AnnotationRetention.RUNTIME)
annotation class NamedComputation

@Qualifier
@Retention(value = AnnotationRetention.RUNTIME)
annotation class NamedUI()

