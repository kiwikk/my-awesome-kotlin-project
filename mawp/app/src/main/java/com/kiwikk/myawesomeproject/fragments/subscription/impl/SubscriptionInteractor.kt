package com.kiwikk.myawesomeproject.fragments.subscription.impl

import android.content.Context
import android.util.Log
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.firebase.FirebaseApp
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.kiwikk.myawesomeproject.callbacks.OnGetDoneTasksCallback
import com.kiwikk.myawesomeproject.callbacks.OnGetNotDoneTasksCallback
import com.kiwikk.myawesomeproject.callbacks.OnSearchCallback
import com.kiwikk.myawesomeproject.database.relation.IRelationDbRep
import com.kiwikk.myawesomeproject.database.taskdb.IDBTaskRepository
import com.kiwikk.myawesomeproject.fragments.subscription.ISubscriptionInteractor
import javax.inject.Inject

class SubscriptionInteractor @Inject constructor(context: Context,
                                                 val db: IRelationDbRep,
                                                 val dbTaskRepository: IDBTaskRepository) : ISubscriptionInteractor {
    var mFirebaseDatabase: FirebaseDatabase
    var mDatabaseReference: DatabaseReference
    val account = GoogleSignIn.getLastSignedInAccount(context)?.email


    init {
        FirebaseApp.initializeApp(context)
        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mFirebaseDatabase.reference
    }

    override fun addSubscribe(email: String) {
        if (account != null) {
            Log.i("Subscribe", "$account on $email")
            db.subscribe(account, email)
        }

    }

    override fun removeSubscribe(email: String) {
        Log.i("account unsybscribe", account.toString())
        if (account != null) {
            Log.i("Unubscribe", "$account from $email")
            db.unsubscribe(account.split("@")[0], email)
        }
    }

    override fun checkSubscribe(email: String, onSearchCallback: OnSearchCallback) {
        return db.checkSubscribe(account!!.split("@")[0], email.split("@")[0], onSearchCallback)
    }

    override fun getDoneTasks(userName: String, onGetDoneTasksCallback: OnGetDoneTasksCallback) {
        dbTaskRepository.getDoneTasks(userName, onGetDoneTasksCallback)
    }

    override fun getNotDoneTasks(userName: String, onGetDoneTasksCallback: OnGetNotDoneTasksCallback) {
        dbTaskRepository.getNotDoneTasks(userName, onGetDoneTasksCallback)
    }
}