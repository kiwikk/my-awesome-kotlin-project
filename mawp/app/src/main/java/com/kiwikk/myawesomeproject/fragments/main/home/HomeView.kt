package com.kiwikk.myawesomeproject.fragments.main.home

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface HomeView : MvpView {
    fun createTable()
    fun colorWeeks(weeks: Int)
    fun setPersonBDate()
}