package com.kiwikk.myawesomeproject.fragments.subscription.impl

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.FirebaseApp
import com.google.firebase.database.*
import com.kiwikk.myawesomeproject.R
import com.kiwikk.myawesomeproject.activities.subscriptions.SubscriptionActivity
import com.kiwikk.myawesomeproject.adapters.RecyclerViewAdapter
import com.kiwikk.myawesomeproject.database.relation.impl.RelationDBRep
import com.kiwikk.myawesomeproject.dataclasses.Person
import com.kiwikk.myawesomeproject.fragments.subscription.ISubscriptionInteractor
import javax.inject.Inject

class SubscriptionsFragment : Fragment() {

    @Inject
    lateinit var interactor: ISubscriptionInteractor

    lateinit var mFirebaseDatabase: FirebaseDatabase
    lateinit var mDatabaseReference: DatabaseReference
    private val usersList = ArrayList<Person>()
    lateinit var recyclerView: RecyclerView
    lateinit var googleSignInAccount: GoogleSignInAccount


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_subscription, container, false)
        (activity as SubscriptionActivity).component.inject(this)


        val searchView = view.findViewById<SearchView>(R.id.sub_search)
        val header = view.findViewById<TextView>(R.id.sub_tv)
        searchView.visibility = View.GONE
        header.text = "Subscriptions"

        initFirebase()
        googleSignInAccount = GoogleSignIn.getLastSignedInAccount(requireActivity())!!
        addChildEventLisstener()

        recyclerView = view.findViewById(R.id.sub_recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = RecyclerViewAdapter(usersList, interactor)

        return view
    }


    private fun addChildEventLisstener() {
        mDatabaseReference
                .child(RelationDBRep.SUBSCRIPTIONS)
                .child(googleSignInAccount.email!!.split("@")[0])
                .addChildEventListener(object : ChildEventListener {
                    override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                        usersList.clear()
                        Log.i("info from firebase", snapshot.childrenCount.toString())
                        val userName = snapshot.getValue(String::class.java)
                        getUser(userName ?: "null")
                    }

                    override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                        TODO("Not yet implemented")
                    }

                    override fun onChildRemoved(snapshot: DataSnapshot) {
                        Log.i("info from removed firebase", snapshot.toString())
                        val userName = snapshot.getValue(String::class.java)
                        usersList.removeIf {
                            it.name == userName
                        }
                        recyclerView.adapter!!.notifyDataSetChanged()
                    }

                    override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                        TODO("Not yet implemented")
                    }

                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }

                })
    }

    private fun getUser(user: String) {
        mDatabaseReference
                .child(RelationDBRep.USER)
                .child(user.split("@")[0])
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val person = snapshot.getValue(Person::class.java)
                        Log.i("Get person from Firebase", person.toString())
                        if (person != null) {
                            usersList.add(person)
                            recyclerView.adapter!!.notifyDataSetChanged()
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }
                })
    }

    private fun initFirebase() {
        FirebaseApp.initializeApp(context!!)
        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mFirebaseDatabase.reference
    }
}