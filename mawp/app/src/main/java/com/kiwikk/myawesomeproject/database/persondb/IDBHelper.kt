package com.kiwikk.myawesomeproject.database.persondb

import android.database.sqlite.SQLiteDatabase
import com.kiwikk.myawesomeproject.dataclasses.Person

interface IDBHelper {
    fun insertPerson(person: Person, db:SQLiteDatabase) :Long
    fun updateDB(person: Person, db:SQLiteDatabase)
    fun getReadebleDB():SQLiteDatabase
    fun getWritableDB():SQLiteDatabase
}