package com.kiwikk.myawesomeproject.elements

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.checkbox.MaterialCheckBox
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.kiwikk.myawesomeproject.R
import com.kiwikk.myawesomeproject.dataclasses.Task
import com.kiwikk.myawesomeproject.fragments.main.home.IHomeInteractor
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class CreateTaskDialog(val tasks: ArrayList<Task>,
                       val recyclerView: RecyclerView,
                       val weekId: Int,
                       val personName: String,
                       val interactor: IHomeInteractor,
                       val date:String) : DialogFragment() {

    lateinit var vview: View
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val inflater = activity!!.layoutInflater
        val view = inflater.inflate(R.layout.create_task_layout, null)

        val menu = view.findViewById<TextInputLayout>(R.id.task_menu)
        val items = listOf(getString(R.string.str_no_type), getString(R.string.str_task_study), getString(R.string.str_task_work), getString(R.string.str_task_personal))
        val adapter = ArrayAdapter(requireContext(), R.layout.list_item, items)

        (menu.editText as? AutoCompleteTextView)?.setAdapter(adapter)

        vview = view
        builder.setView(view)
        builder.setPositiveButton("Готово", this::onTaskCreated)

        return builder.create()
    }

    fun onTaskCreated(dialogInterface: DialogInterface, i: Int) {
        val name = vview.findViewById<TextInputEditText>(R.id.tv_name).text.toString()
        val description = vview.findViewById<TextInputEditText>(R.id.tv_description).text.toString()
        val type = vview.findViewById<AutoCompleteTextView>(R.id.task_menu_tv).text.toString()
        val private = vview.findViewById<MaterialCheckBox>(R.id.cb_private).isChecked

        tasks.add(Task(person_name = personName.split('@')[0],
                week_id = weekId.toString(),
                name = name,
                description = description,
                type = if (type == "") "Без типа" else type,
                isPrivate = private,
                date = date,
                isDone = false,
                likes = 0))

        interactor.insertTask(tasks.last())
        recyclerView.adapter!!.notifyDataSetChanged()
    }

}