package com.kiwikk.myawesomeproject.fragments.main.settings.impl

import android.util.Log
import com.kiwikk.myawesomeproject.callbacks.OnGetDoneTasksCallback
import com.kiwikk.myawesomeproject.callbacks.OnGetNotDoneTasksCallback
import com.kiwikk.myawesomeproject.di.named.NamedUI
import com.kiwikk.myawesomeproject.fragments.main.settings.ISettingsInteractor
import com.kiwikk.myawesomeproject.fragments.main.settings.ISettingsPresenter
import com.kiwikk.myawesomeproject.fragments.main.settings.SettingsView
import com.kiwikk.myawesomeproject.dataclasses.Person
import moxy.InjectViewState
import moxy.MvpPresenter
import rx.Scheduler
import rx.subscriptions.CompositeSubscription
import javax.inject.Inject

class SettingsPresenter @Inject constructor(val interactor: ISettingsInteractor,
                                            @NamedUI val scheduler: Scheduler) : MvpPresenter<SettingsView>(), ISettingsPresenter {

    private lateinit var person: Person
    private val compositeSubscription = CompositeSubscription()

    init {
        val sub = interactor.getPerson()
                .observeOn(scheduler)
                .subscribe {
                    person = it
                    viewState.setBDate(person.birthDate)
                    Log.i("SettingsPres", "$it")
                }
        compositeSubscription.add(sub)
    }

    override fun setNewBDate() {
        val obs = interactor.getPerson()
                .observeOn(scheduler)
                .subscribe {
                    viewState.setNewBDate(it)
                }
        compositeSubscription.add(obs)
    }


    override fun showDonut() {
        viewState.showDonut()
    }

    override fun hideDonut() {
        viewState.hideDonnut()
    }

    override fun updateBDate(bDate: String) {
        person.birthDate=bDate
        val completable = interactor.updateDB(person)
        val subscriber = completable.observeOn(scheduler)
                .subscribe(
                        {
                            viewState.showUpdateResult("Success bDate")
                            viewState.setBDate(person.birthDate)
                        },
                        { t ->
                            viewState.showUpdateResult("Unsuccess bDate")
                            t.printStackTrace()
                        }
                )
        compositeSubscription.add(subscriber)
    }

    override fun getDoneTasks(userName: String, onGetDoneTasksCallback: OnGetDoneTasksCallback) {
        interactor.getDoneTasks(userName, onGetDoneTasksCallback)
    }

    override fun getNotDoneTasks(userName: String, onGetDoneTasksCallback: OnGetNotDoneTasksCallback) {
        interactor.getNotDoneTasks(userName, onGetDoneTasksCallback)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeSubscription.unsubscribe()
    }
}

