package com.kiwikk.myawesomeproject.fragments.subscription

import com.kiwikk.myawesomeproject.callbacks.OnGetDoneTasksCallback
import com.kiwikk.myawesomeproject.callbacks.OnGetNotDoneTasksCallback
import com.kiwikk.myawesomeproject.callbacks.OnSearchCallback

interface ISubscriptionInteractor {
    fun addSubscribe(email: String)
    fun removeSubscribe(email: String)
    fun checkSubscribe(email: String, onSearchCallback: OnSearchCallback)
    fun getDoneTasks(userName: String, onGetDoneTasksCallback: OnGetDoneTasksCallback)
    fun getNotDoneTasks(userName: String, onGetDoneTasksCallback: OnGetNotDoneTasksCallback)
}