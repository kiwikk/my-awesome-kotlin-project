package com.kiwikk.myawesomeproject.database.persondb.impl

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteException
import android.os.Looper
import android.util.Log
import com.google.firebase.FirebaseApp
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.kiwikk.myawesomeproject.database.persondb.IDBHelper
import com.kiwikk.myawesomeproject.database.persondb.IDBRepository
import com.kiwikk.myawesomeproject.di.named.NamedComputation
import com.kiwikk.myawesomeproject.dataclasses.Person
import rx.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

import javax.inject.Inject

class DBRepository @Inject constructor(val dbHelper: IDBHelper,
                                       @NamedComputation val subOnScheduler: Scheduler,
                                       context: Context) : IDBRepository {
    private val readableDB = dbHelper.getReadebleDB()
    private val writableDB = dbHelper.getWritableDB()
    var id = 1L
    var mFirebaseDatabase: FirebaseDatabase
    var mDatabaseReference: DatabaseReference

    init {
        FirebaseApp.initializeApp(context)
        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mFirebaseDatabase.getReference("users")
    }

    private fun getPersonFromDB(): Person {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw RuntimeException()
        }


        var person = Person(weeks = 0, imageUrl = "")
        Log.i("birth day", person.birthDate)

        try {
            val cursor = openCursor(id)

            if (cursor!!.moveToFirst()) {
                val idIndex = cursor.getColumnIndex(DataBaseHelper.ID)
                val nameIndex = cursor.getColumnIndex(DataBaseHelper.NAME)
                val bDateIndex = cursor.getColumnIndex(DataBaseHelper.DATE_OF_BIRTH)
                val imageUrl = cursor.getColumnIndex(DataBaseHelper.IMAGE_URL)

                Log.d("Colums:", "id: ${idIndex}, name: ${nameIndex}, bDate: $bDateIndex, image: $imageUrl")
                Log.d("Data:", "id: ${cursor.getInt(idIndex)}, name: ${cursor.getString(nameIndex)}, bDate: ${cursor.getString(bDateIndex)}")

                val bday = cursor.getString(bDateIndex)

                val dtf = DateTimeFormatter.ofPattern("dd MM yyyy")
                val birthDateLocal = LocalDate.parse(bday, dtf)

                val now = LocalDate.now()
                val weeks = ChronoUnit.WEEKS.between(birthDateLocal, now).toInt()

                person = Person(cursor.getInt(idIndex), cursor.getString(nameIndex), cursor.getString(bDateIndex), cursor.getString(imageUrl), weeks = weeks)
                cursor.close()
            }
        } catch (e: SQLiteException) {
            e.printStackTrace()
        }

        return person
    }

    private fun openCursor(id: Long): Cursor? {
        return readableDB.query(DataBaseHelper.DB_NAME, arrayOf<String?>("_id", DataBaseHelper.NAME, DataBaseHelper.DATE_OF_BIRTH, DataBaseHelper.IMAGE_URL),
                "_id = $id", null,
                null, null, null)
    }

    override fun insertPerson(person: Person): Single<Long> {
        Log.i("DBRepository", "insertPerson $person")

        mDatabaseReference
                .child(person.name.split('@')[0])
                .setValue(person)

        return Single.fromCallable {
            dbHelper.insertPerson(person, writableDB)
        }.subscribeOn(subOnScheduler)
    }

    override fun updatePerson(person: Person): Completable {
        Log.i("DBRepository", "updateDB $person")

        mDatabaseReference
                .child(person.name.split("@")[0])
                .setValue(person)


        return Completable.fromCallable {
            dbHelper.updateDB(person, writableDB)
        }.subscribeOn(subOnScheduler)
    }

    override fun getPerson(): Observable<Person> {
        return Observable.fromCallable {
            getPersonFromDB()
        }.subscribeOn(subOnScheduler)
    }



    override fun closeDB() {
        readableDB.close()
        writableDB.close()
    }
}
