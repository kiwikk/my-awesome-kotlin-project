package com.kiwikk.myawesomeproject.database.relation.impl

import android.content.Context
import android.util.Log
import com.google.firebase.FirebaseApp
import com.google.firebase.database.*
import com.kiwikk.myawesomeproject.callbacks.OnSearchCallback
import com.kiwikk.myawesomeproject.database.relation.IRelationDbRep
import javax.inject.Inject

class RelationDBRep @Inject constructor(context: Context) : IRelationDbRep {
    var mFirebaseDatabase: FirebaseDatabase
    var mDatabaseReference: DatabaseReference

    init {
        FirebaseApp.initializeApp(context)
        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mFirebaseDatabase.reference
    }

    override fun subscribe(from: String, to: String) {
        mDatabaseReference.child(SUBSCRIPTIONS)
                .child(from.split("@")[0])
                .push()
                .setValue(to.split("@")[0])

        mDatabaseReference.child(SUBSCRIBERS)
                .child(to.split("@")[0])
                .push()
                .setValue(from.split("@")[0])
    }

    override fun unsubscribe(from: String, to: String) {
        mDatabaseReference.child(SUBSCRIPTIONS)
                .child(from)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        snapshot.children.filter {
                            it.getValue(String::class.java) == to
                        }.forEach {
                            Log.i("in unsubscribe", "$it")
                            it.ref.removeValue()
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }

                })

        mDatabaseReference.child(SUBSCRIBERS)
                .child(to)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        snapshot.children.filter {
                            Log.i("remove", "${it.getValue(String::class.java) == from}")
                            it.getValue(String::class.java) == from
                        }.forEach {
                            it.ref.removeValue()
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }

                })
    }

    override fun checkSubscribe(from: String, to: String, onSearchCallback: OnSearchCallback) {
        mDatabaseReference.child(SUBSCRIPTIONS)
                .child(from)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        Log.i("check $to in $from", snapshot.children.any { it.getValue(String::class.java) == to }.toString())
                        onSearchCallback.onResult(snapshot.children.any { it.getValue(String::class.java) == to })
                    }

                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }

                })
    }

    companion object {
        const val SUBSCRIPTIONS = "subscriptions"
        const val SUBSCRIBERS = "subscribers"
        const val TASKS = "tasks"
        const val USER = "users"
    }
}