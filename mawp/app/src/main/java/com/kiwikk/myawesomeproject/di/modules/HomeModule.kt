package com.kiwikk.myawesomeproject.di.modules

import com.kiwikk.myawesomeproject.di.scopes.MainActivityScope
import com.kiwikk.myawesomeproject.fragments.main.home.HomeView
import com.kiwikk.myawesomeproject.fragments.main.home.impl.HomePresenter
import com.kiwikk.myawesomeproject.fragments.main.home.IHomeInteractor
import com.kiwikk.myawesomeproject.fragments.main.home.impl.HomeInteractor
import dagger.Binds
import dagger.Module
import moxy.MvpPresenter

@Module
interface HomeModule {
    @Binds
    @MainActivityScope
    fun provideHomeInteractor(homeInteractor: HomeInteractor) : IHomeInteractor

    @Binds
    @MainActivityScope
    fun providePresenter(presenter: HomePresenter): MvpPresenter<HomeView>
}