package com.kiwikk.myawesomeproject.fragments.main.settings

import com.kiwikk.myawesomeproject.dataclasses.Person
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface SettingsView : MvpView {
    fun showDonut()
    fun hideDonnut()
    fun setNewBDate(person: Person)
    fun showUpdateResult(text: String)
    fun setBDate(bDate: String)
}