package com.kiwikk.myawesomeproject.fragments.main.news

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.FirebaseApp
import com.google.firebase.database.*
import com.kiwikk.myawesomeproject.activities.main.MainActivity
import com.kiwikk.myawesomeproject.R
import com.kiwikk.myawesomeproject.database.relation.impl.RelationDBRep
import com.kiwikk.myawesomeproject.dataclasses.Task
import com.kiwikk.myawesomeproject.adapters.TaskRecyclerViewAdapter
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 * Use the [NewsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NewsFragment : Fragment() {

    private lateinit var vview: View
    private lateinit var account: String
    private val tasks = ArrayList<Task>()
    lateinit var mFirebaseDatabase: FirebaseDatabase
    lateinit var mDatabaseReference: DatabaseReference
    lateinit var recyclerView:RecyclerView
    lateinit var textView: TextView

    @Inject
    lateinit var interactor: NewsInteractor

    override fun onCreate(savedInstanceState: Bundle?) {
        (activity as MainActivity).component.inject(this)
        account = (activity as MainActivity).googleSignInAccount!!.email!!.split("@")[0]
        initFB()

        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        vview = inflater.inflate(R.layout.fragment_news, container, false)
        textView = vview.findViewById(R.id.news_text)
        recyclerView = vview.findViewById<RecyclerView>(R.id.news_recyclerview)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = TaskRecyclerViewAdapter(tasks, interactor)
        addEventFirebaseListener()

        return vview
    }

    private fun addEventFirebaseListener() {
        Log.i("accountNews", account)
        mDatabaseReference
                .child(RelationDBRep.SUBSCRIPTIONS)
                .child(account)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        if (tasks.isNotEmpty()) tasks.clear()
                        Log.i("subscriptions", snapshot.childrenCount.toString())
                        for (postSnapshot in snapshot.children) {
                            val userName = postSnapshot.getValue(String::class.java)
                            getTask(userName ?: "null")
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }

                })
    }

    private fun getTask(user: String) {
        mDatabaseReference
                .child(RelationDBRep.TASKS)
                .child(user.split("@")[0])
                .orderByChild("private")
                .equalTo(false)
                .addChildEventListener(object : ChildEventListener {
                    override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                        Log.i("subscriptions tasks:", snapshot.childrenCount.toString())
                            val task = snapshot.getValue(Task::class.java)
                            Log.i("Get task from Firebase", task.toString())
                            if (task != null) {
                                tasks.add(0,task)
                            }
                        recyclerView.adapter!!.notifyDataSetChanged()
                        if(tasks.isNotEmpty()) textView.visibility = View.GONE
                        else textView.visibility = View.VISIBLE
                    }

                    override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                        Log.i("subscriptions tasks:", snapshot.childrenCount.toString())
                        val task = snapshot.getValue(Task::class.java)
                        Log.i("Get task from change Firebase", task.toString())
                        if (task != null) {
                            tasks.add(0,task)
                        }
                        recyclerView.adapter!!.notifyDataSetChanged()
                        if(tasks.isNotEmpty()) textView.visibility = View.GONE
                        else textView.visibility = View.VISIBLE
                    }

                    override fun onChildRemoved(snapshot: DataSnapshot) {
                        Log.i("subscriptions tasks:", snapshot.childrenCount.toString())
                        val task = snapshot.getValue(Task::class.java)
                        Log.i("Get task from removed Firebase", task.toString())
                        if (task != null) {
                            tasks.remove(task)
                        }
                        recyclerView.adapter!!.notifyDataSetChanged()
                        if(tasks.isNotEmpty()) textView.visibility = View.GONE
                        else textView.visibility = View.VISIBLE
                    }

                    override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                        TODO("Not yet implemented")
                    }

                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }
                })
    }


    private fun initFB() {
        FirebaseApp.initializeApp(requireContext())
        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mFirebaseDatabase.reference
    }

    companion object {
        private val ARG_PARAM1: String = "param1"
        private val ARG_PARAM2: String = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment NotificationsFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String?, param2: String?): NewsFragment? {
            val fragment = NewsFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}