package com.kiwikk.myawesomeproject.callbacks

interface OnLikeCallback {
    fun onLike(likes:Int)
}