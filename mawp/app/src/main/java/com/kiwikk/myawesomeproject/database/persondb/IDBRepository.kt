package com.kiwikk.myawesomeproject.database.persondb

import com.kiwikk.myawesomeproject.dataclasses.Person
import com.kiwikk.myawesomeproject.dataclasses.Task
import rx.Completable
import rx.Observable
import rx.Single

interface IDBRepository {
    fun getPerson(): Observable<Person>
    fun insertPerson(person: Person): Single<Long>
    fun updatePerson(person: Person): Completable
    fun closeDB()
}