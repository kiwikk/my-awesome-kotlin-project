package com.kiwikk.myawesomeproject.fragments.main.settings

import com.kiwikk.myawesomeproject.callbacks.OnGetDoneTasksCallback
import com.kiwikk.myawesomeproject.callbacks.OnGetNotDoneTasksCallback

interface ISettingsPresenter {
    fun setNewBDate()
    fun showDonut()
    fun hideDonut()
    fun updateBDate(bDate:String)
    fun getDoneTasks(userName:String, onGetDoneTasksCallback: OnGetDoneTasksCallback)
    fun getNotDoneTasks(userName:String, onGetDoneTasksCallback: OnGetNotDoneTasksCallback)
}