package com.kiwikk.myawesomeproject.di.modules

import com.kiwikk.myawesomeproject.database.persondb.IDBHelper
import com.kiwikk.myawesomeproject.database.persondb.impl.DBRepository
import com.kiwikk.myawesomeproject.database.persondb.IDBRepository
import com.kiwikk.myawesomeproject.database.persondb.impl.DataBaseHelper
import com.kiwikk.myawesomeproject.database.taskdb.IDBTaskHelper
import com.kiwikk.myawesomeproject.database.taskdb.IDBTaskRepository
import com.kiwikk.myawesomeproject.database.taskdb.impl.DBTaskRepository
import com.kiwikk.myawesomeproject.database.taskdb.impl.DataBaseTaskHelper
import com.kiwikk.myawesomeproject.di.scopes.ApplicationScope
import dagger.Binds
import dagger.Module;

@Module
interface DBRepModule {
    @Binds
    @ApplicationScope
    fun provideDBRep(dbRepository: DBRepository) : IDBRepository

    @Binds
    @ApplicationScope
    fun provideDB(db: DataBaseHelper) : IDBHelper

    @Binds
    @ApplicationScope
    fun provideTaskDB(db: DataBaseTaskHelper) : IDBTaskHelper

    @Binds
    @ApplicationScope
    fun provide(dbRep: DBTaskRepository) : IDBTaskRepository
}
