package com.kiwikk.myawesomeproject.dataclasses

data class Task(
        var id: Int = -1,
        var person_name: String = "",
        var week_id: String = "",
        var name: String = "",
        var description: String = "",
        var type: String = "",
        var isPrivate: Boolean = false,
        var date: String = "",
        var isDone: Boolean = false,
        var likes: Int = 0
)
