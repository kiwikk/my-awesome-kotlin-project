package com.kiwikk.myawesomeproject.activities.main.mvp

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface MainView:MvpView {
    fun beginHomeTransaction()
    fun beginNotifsTransaction()
    fun beginSettingsTransaction()
}