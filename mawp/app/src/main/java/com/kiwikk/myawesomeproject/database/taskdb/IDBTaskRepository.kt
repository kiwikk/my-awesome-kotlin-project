package com.kiwikk.myawesomeproject.database.taskdb

import com.kiwikk.myawesomeproject.callbacks.OnGetDoneTasksCallback
import com.kiwikk.myawesomeproject.callbacks.OnGetNotDoneTasksCallback
import com.kiwikk.myawesomeproject.callbacks.OnLikeCallback
import com.kiwikk.myawesomeproject.dataclasses.Task
import rx.Completable
import rx.Single

interface IDBTaskRepository {
    fun insertTask(task: Task): Single<Long>
    fun closeDB()
    fun taskIsDone(task: Task): Completable
    fun setLikes(task: Task, onLikeCallback: OnLikeCallback)
    fun getDoneTasks(userName:String, onDoneTasks: OnGetDoneTasksCallback)
    fun getNotDoneTasks(userName: String, onNotDoneTasks: OnGetNotDoneTasksCallback)
}