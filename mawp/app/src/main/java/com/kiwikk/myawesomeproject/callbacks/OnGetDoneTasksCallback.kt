package com.kiwikk.myawesomeproject.callbacks

interface OnGetDoneTasksCallback {
    fun getDoneTasks(tasks:Long)
}