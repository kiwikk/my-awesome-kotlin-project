package com.kiwikk.myawesomeproject.fragments.main.home

import com.kiwikk.myawesomeproject.dataclasses.Person
import com.kiwikk.myawesomeproject.dataclasses.Task
import com.kiwikk.myawesomeproject.fragments.Interactor
import rx.Observable
import rx.Single

interface IHomeInteractor : Interactor {
    fun insertPersonToDB(person: Person): Single<Long>
    fun closeDB()
    fun getPerson(): Observable<Person>
    fun insertTask(task: Task)
}