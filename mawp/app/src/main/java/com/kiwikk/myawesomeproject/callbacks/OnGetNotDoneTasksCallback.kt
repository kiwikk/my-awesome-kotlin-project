package com.kiwikk.myawesomeproject.callbacks

interface OnGetNotDoneTasksCallback {
    fun getNotDoneTasks(tasks: Long)
}