package com.kiwikk.myawesomeproject.fragments.main.settings

import com.kiwikk.myawesomeproject.callbacks.OnGetDoneTasksCallback
import com.kiwikk.myawesomeproject.callbacks.OnGetNotDoneTasksCallback
import com.kiwikk.myawesomeproject.dataclasses.Person
import rx.Completable
import rx.Observable

interface ISettingsInteractor {
    fun updateDB(person: Person): Completable
    fun getPerson():Observable<Person>
    fun getDoneTasks(userName:String, onGetDoneTasksCallback: OnGetDoneTasksCallback)
    fun getNotDoneTasks(userName:String, onGetDoneTasksCallback: OnGetNotDoneTasksCallback)
}