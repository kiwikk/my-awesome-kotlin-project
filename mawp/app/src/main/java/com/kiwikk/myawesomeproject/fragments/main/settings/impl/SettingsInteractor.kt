package com.kiwikk.myawesomeproject.fragments.main.settings.impl

import com.kiwikk.myawesomeproject.callbacks.OnGetDoneTasksCallback
import com.kiwikk.myawesomeproject.callbacks.OnGetNotDoneTasksCallback
import com.kiwikk.myawesomeproject.database.persondb.IDBRepository
import com.kiwikk.myawesomeproject.database.taskdb.IDBTaskRepository
import com.kiwikk.myawesomeproject.fragments.main.settings.ISettingsInteractor
import com.kiwikk.myawesomeproject.dataclasses.Person
import rx.Completable
import rx.Observable
import javax.inject.Inject

class SettingsInteractor @Inject constructor(val dbTask:IDBTaskRepository) : ISettingsInteractor {
    @Inject
    lateinit var db: IDBRepository

    override fun updateDB(person: Person): Completable {
        return db.updatePerson(person)
    }

    override fun getPerson(): Observable<Person> {
        return db.getPerson()
    }

    override fun getDoneTasks(userName: String, onGetDoneTasksCallback: OnGetDoneTasksCallback) {
        dbTask.getDoneTasks(userName, onGetDoneTasksCallback)

    }

    override fun getNotDoneTasks(userName: String, onGetDoneTasksCallback: OnGetNotDoneTasksCallback) {
        dbTask.getNotDoneTasks(userName, onGetDoneTasksCallback)
    }
}
