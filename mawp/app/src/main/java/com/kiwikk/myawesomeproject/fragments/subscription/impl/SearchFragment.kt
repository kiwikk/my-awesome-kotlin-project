package com.kiwikk.myawesomeproject.fragments.subscription.impl

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.FirebaseApp
import com.google.firebase.database.*
import com.kiwikk.myawesomeproject.R
import com.kiwikk.myawesomeproject.activities.subscriptions.SubscriptionActivity
import com.kiwikk.myawesomeproject.dataclasses.Person
import com.kiwikk.myawesomeproject.adapters.RecyclerViewAdapter
import javax.inject.Inject

class SearchFragment : Fragment() {
    @Inject
    lateinit var interactor: SubscriptionInteractor


    lateinit var mFirebaseDatabase: FirebaseDatabase
    lateinit var mDatabaseReference: DatabaseReference
    private val usersList = ArrayList<Person>()
    lateinit var recyclerView: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_subscription, container, false)
        (activity as SubscriptionActivity).component.inject(this)

        val searchView = view.findViewById<SearchView>(R.id.sub_search)
        val header = view.findViewById<TextView>(R.id.sub_tv)
        val image = view.findViewById<ImageView>(R.id.sub_separator)
        header.visibility = View.GONE
        image.visibility = View.GONE
        searchView.isIconifiedByDefault = false
        searchView.requestFocusFromTouch()
        searchView.onActionViewExpanded()

        initFirebase()

        recyclerView = view.findViewById<RecyclerView>(R.id.sub_recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = RecyclerViewAdapter(usersList, interactor)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                Log.i("SearchView", searchView.query.toString())
                firebaseUserSearch(searchView.query.toString())
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                if (p0.isNullOrEmpty()) {
                    usersList.clear()
                    recyclerView.adapter!!.notifyDataSetChanged()
                }
                return true
            }

        })
        return view
    }

    private fun firebaseUserSearch(query: String) {
        mDatabaseReference
                .child("users")
                .child(query.split("@")[0])
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        if (usersList.isNotEmpty()) usersList.clear()
                        val person = snapshot.getValue(Person::class.java)
                        Log.i("Get person from Firebase", person.toString())

                        if (person != null) {
                            usersList.add(person)
                            recyclerView.adapter!!.notifyDataSetChanged()
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }

                })
    }

    private fun initFirebase() {
        FirebaseApp.initializeApp(context!!)
        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mFirebaseDatabase.reference
    }

}