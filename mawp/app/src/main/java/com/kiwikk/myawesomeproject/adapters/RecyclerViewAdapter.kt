package com.kiwikk.myawesomeproject.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.ToggleButton
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kiwikk.myawesomeproject.R
import com.kiwikk.myawesomeproject.callbacks.OnGetDoneTasksCallback
import com.kiwikk.myawesomeproject.callbacks.OnGetNotDoneTasksCallback
import com.kiwikk.myawesomeproject.callbacks.OnSearchCallback
import com.kiwikk.myawesomeproject.dataclasses.Person
import com.kiwikk.myawesomeproject.fragments.subscription.ISubscriptionInteractor

class RecyclerViewAdapter(val users: ArrayList<Person>,
                          val interactor: ISubscriptionInteractor) : RecyclerView.Adapter<RecyclerViewAdapter.UsersViewHolder>() {
    class UsersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var emailTV: TextView = itemView.findViewById(R.id.profile_sub_email)
        var tvTasksDone: TextView = itemView.findViewById(R.id.tv_tasks_sub_done)
        var tvTasksNotDone: TextView = itemView.findViewById(R.id.tv_tasks_sub_not_done)
        var image: ImageView = itemView.findViewById(R.id.profile_sub_image)
        var subscribe: ToggleButton = itemView.findViewById(R.id.subscribe_button)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.subscription_cardview, parent, false)
        return UsersViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        holder.emailTV.text = users[position].name
        Glide.with(holder.itemView).load(users[position].imageUrl).into(holder.image)

        interactor.getDoneTasks(users[position].name.split("@")[0], object : OnGetDoneTasksCallback {
            override fun getDoneTasks(tasks: Long) {
                holder.tvTasksDone.text = "Сделано: $tasks"
            }
        })

        interactor.getNotDoneTasks(users[position].name.split("@")[0], object : OnGetNotDoneTasksCallback {
            override fun getNotDoneTasks(tasks: Long) {
                holder.tvTasksNotDone.text = "Не сделано: $tasks"
            }

        })

        interactor.checkSubscribe(users[position].name, object : OnSearchCallback {
            override fun onResult(res: Boolean) {
                holder.subscribe.isChecked = res
            }
        })

        holder.subscribe.setOnClickListener {
            if (users.isNotEmpty()) {
                interactor.checkSubscribe(users[position].name, object : OnSearchCallback {
                    override fun onResult(res: Boolean) {
                        Log.i("subscribe button", "ischecked = ${holder.subscribe.isChecked}, res = ${res}")
                        if (holder.subscribe.isChecked && !res)
                            interactor.addSubscribe(users[position].name.split("@")[0])
                        else interactor.removeSubscribe(users[position].name.split("@")[0])
                    }
                })
            }
        }
    }

    override fun getItemCount() = users.size
}