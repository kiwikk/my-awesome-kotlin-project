package com.kiwikk.myawesomeproject

import android.app.Application
import android.util.Log
import com.kiwikk.myawesomeproject.di.components.AppComponent
import com.kiwikk.myawesomeproject.di.components.DaggerAppComponent
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class AndroidApplication : Application() {
    lateinit var component: AppComponent
        private set

    override fun onCreate() {
        super.onCreate()
        component = buildComponent()
    }

    protected fun buildComponent(): AppComponent {
        Log.d("appContext: ", applicationContext.toString())
        return DaggerAppComponent.builder()
                .bindContext(this)
                .computationScheduler(Schedulers.computation())
                .UIScheduler(AndroidSchedulers.mainThread())
                .build()
    }
}