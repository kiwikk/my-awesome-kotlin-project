package com.kiwikk.myawesomeproject.activities.main

import android.os.Bundle
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.ismaeldivita.chipnavigation.ChipNavigationBar
import com.kiwikk.myawesomeproject.AndroidApplication
import com.kiwikk.myawesomeproject.R
import com.kiwikk.myawesomeproject.di.components.ActivityComponent
import com.kiwikk.myawesomeproject.fragments.main.home.impl.HomeFragment
import com.kiwikk.myawesomeproject.fragments.main.news.NewsFragment
import com.kiwikk.myawesomeproject.fragments.main.settings.impl.SettingsFragment
import com.kiwikk.myawesomeproject.activities.main.mvp.MainPresenter
import com.kiwikk.myawesomeproject.activities.main.mvp.MainView
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class MainActivity : MvpAppCompatActivity(), MainView {
    lateinit var component: ActivityComponent
        private set

    private var chipNavigationBar: ChipNavigationBar? = null

    var googleSignInAccount: GoogleSignInAccount? = null

    lateinit var homeFragment: HomeFragment
    lateinit var newsFragment: NewsFragment
    lateinit var settingsFragment: SettingsFragment

    @InjectPresenter
    lateinit var mPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        component = (application as AndroidApplication).component.activityComponent()
        super.onCreate(savedInstanceState)


        googleSignInAccount = GoogleSignIn.getLastSignedInAccount(this)

        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        initFragments()

        chipNavigationBar = findViewById(R.id.chipNavigation)
        chipNavigationBar?.setItemSelected(R.id.home, true)

        chipNavigationBar?.setOnItemSelectedListener(object : ChipNavigationBar.OnItemSelectedListener {
            override fun onItemSelected(id: Int) {
                when (id) {
                    R.id.home -> mPresenter.beginHomeTransaction()
                    R.id.notifications -> mPresenter.beginNotifsTransaction()
                    R.id.settings -> mPresenter.beginSettingsTransaction()
                }
            }
        })
    }

    fun initFragments() {
        homeFragment = HomeFragment()
        newsFragment = NewsFragment()
        settingsFragment = SettingsFragment()
    }

    override fun beginHomeTransaction() {
        supportFragmentManager.beginTransaction().replace(R.id.container, homeFragment).commit()
    }

    override fun beginNotifsTransaction() {
        supportFragmentManager.beginTransaction().replace(R.id.container, newsFragment).commit()
    }

    override fun beginSettingsTransaction() {
        supportFragmentManager.beginTransaction().replace(R.id.container, settingsFragment).commit()
    }
}