package com.kiwikk.myawesomeproject.elements

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.FirebaseApp
import com.google.firebase.database.*
import com.kiwikk.myawesomeproject.R
import com.kiwikk.myawesomeproject.activities.main.MainActivity
import com.kiwikk.myawesomeproject.adapters.TaskRecyclerViewAdapter
import com.kiwikk.myawesomeproject.database.taskdb.impl.DBTaskRepository
import com.kiwikk.myawesomeproject.dataclasses.Task
import com.kiwikk.myawesomeproject.di.named.NamedComputation
import com.kiwikk.myawesomeproject.fragments.main.home.impl.HomeInteractor
import rx.Scheduler
import rx.subscriptions.CompositeSubscription
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.inject.Inject

class WeekCard(var weekButton: WeekButton) : BottomSheetDialogFragment() {

    lateinit var mFirebaseDatabase: FirebaseDatabase
    lateinit var mDatabaseReference: DatabaseReference


    lateinit var myDate: LocalDate
    lateinit var recyclerView: RecyclerView
    lateinit var personName: String
    lateinit var cardDate: String
    private val tasksList = ArrayList<Task>()

    @Inject
    lateinit var homeInteractor: HomeInteractor

    @Inject
    @NamedComputation
    lateinit var obsOnScheduler: Scheduler
    val compositeSubscription = CompositeSubscription()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.bottom_sheet_layout, container, false)
        (activity as MainActivity).component.inject(this)

        recyclerView = view!!.findViewById(R.id.weekCard_recycleView)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = TaskRecyclerViewAdapter(tasksList, homeInteractor)

        initFirebase()

        compositeSubscription.add(
                homeInteractor.getPerson()
                        .observeOn(obsOnScheduler)
                        .subscribe {
                            personName = it.name
                            val dtf = DateTimeFormatter.ofPattern("dd MM yyyy")
                            val birthDateLocal = LocalDate.parse(it.birthDate, dtf)
                            myDate = birthDateLocal.plusDays(7 * (weekButton.getID()).toLong())
                            updateLayout(it.name)
                        }
        )

        return view
    }

    private fun initFirebase() {
        FirebaseApp.initializeApp(requireContext())
        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mFirebaseDatabase.getReference(DBTaskRepository.TASKS)
    }

    private fun updateLayout(user: String) {
        mDatabaseReference
                .child(user.split("@")[0])
                .orderByChild("week_id")
                .equalTo(weekButton.getID().toString())
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        Log.i("Task in onDataChange", snapshot.toString())
                        if (tasksList.isNotEmpty()) tasksList.clear()
                        for (i in snapshot.children) {
                            val task = i.getValue(Task::class.java)
                            Log.i("Task from firebase", task.toString())
                            if (task != null) {
                                tasksList.add(task)
                                recyclerView.adapter!!.notifyDataSetChanged()
                            }
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.e("DatabaseError", error.message)
                    }

                })
    }

    override fun onStart() {
        super.onStart()
        setMonth()
        addTask()
    }

    private fun addTask() {
        val createButton = view!!.findViewById<ImageButton?>(R.id.addButton)
        createButton!!.setOnClickListener {
            val createTask = CreateTaskDialog(tasksList, recyclerView, weekButton.getID(), personName, homeInteractor, cardDate)
            createTask.show(activity!!.supportFragmentManager, "create_task")
        }
    }

    private fun setMonth() {
        val textView = view!!.findViewById<TextView?>(R.id.dateTextView)
        val month = StringBuilder()
        month.append(myDate.month)
        if (myDate.month != myDate.plusDays(7).month) month.append(" - ").append(myDate.plusDays(7).month)
        month.append(" ").append(myDate.year)
        val days = "${myDate.dayOfMonth} - ${myDate.plusDays(6).dayOfMonth}"
        cardDate = "$days of \n$month"
        textView!!.text = cardDate
        textView.gravity = LinearLayout.TEXT_ALIGNMENT_CENTER
    }

}