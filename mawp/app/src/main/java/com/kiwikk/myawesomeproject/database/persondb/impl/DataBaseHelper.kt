package com.kiwikk.myawesomeproject.database.persondb.impl

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.os.Looper
import android.util.Log
import com.kiwikk.myawesomeproject.database.persondb.IDBHelper
import com.kiwikk.myawesomeproject.dataclasses.Person
import javax.inject.Inject

class DataBaseHelper @Inject constructor(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION), IDBHelper {

    override fun onCreate(db: SQLiteDatabase?) {
        db!!.execSQL("CREATE TABLE $DB_NAME ("
                + "$ID INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "$NAME TEXT, "
                + "$DATE_OF_BIRTH TEXT, "
                + "$IMAGE_URL TEXT);")
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {}

    override fun insertPerson(person: Person, db: SQLiteDatabase): Long {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw RuntimeException()
        }

        val contentValues = ContentValues()

        contentValues.put(NAME, person.name)
        contentValues.put(DATE_OF_BIRTH, person.birthDate)
        contentValues.put(IMAGE_URL, person.imageUrl)

        Log.d("ColumsHelper:", "id: ${person._id}, name: ${person.name}, bDate: ${person.birthDate}")

        return db.insert(DB_NAME, null, contentValues)
    }

    override fun updateDB(person: Person, db: SQLiteDatabase) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw RuntimeException()
        }

        val contentValues = ContentValues()

        contentValues.put(ID, person._id)
        contentValues.put(NAME, person.name)
        contentValues.put(DATE_OF_BIRTH, person.birthDate)
        contentValues.put(IMAGE_URL, person.imageUrl)


        Log.d("id", person.toString())
        db.update(DB_NAME,
                contentValues,
                "_id=${person._id}", null)
    }

    override fun getReadebleDB(): SQLiteDatabase = readableDatabase
    override fun getWritableDB(): SQLiteDatabase = writableDatabase

    companion object {
        const val DB_NAME: String = "PERSON"
        private const val DB_VERSION = 1
        const val ID = "_id"
        const val NAME = "NAME"
        const val DATE_OF_BIRTH = "DATE_OF_BIRTH"
        const val IMAGE_URL = "IMAGE_URL"
    }

}