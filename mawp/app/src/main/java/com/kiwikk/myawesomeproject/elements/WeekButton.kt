package com.kiwikk.myawesomeproject.elements

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.widget.Button
import androidx.fragment.app.FragmentManager
import com.kiwikk.myawesomeproject.R

//у вьюхи 2 назначения: показать (setы и вернуть user interaction)
@SuppressLint("AppCompatCustomView")
class WeekButton : Button {
    private var ID = 0
    private val color: Color? = null
    private var isLived: Boolean? = null
    private var weekCard: WeekCard? = null
    private var fragmentManager: FragmentManager? = null

    constructor(context: Context?, id: Int, fragmentManager: FragmentManager?) : super(context) {
        ID = id
        weekCard = WeekCard(this)
        this.fragmentManager = fragmentManager
        setOnClickListener { weekCard!!.show(fragmentManager!!, weekCard!!.getTag()) }
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    fun getID(): Int {
        return ID
    }

    fun setLived() {
        isLived = true
        setBackgroundResource(R.drawable.ic_cross)
        isEnabled = false
    }

    fun setNotLived(){
        isLived = false
        setBackgroundResource(0)
        isEnabled = true
    }
}