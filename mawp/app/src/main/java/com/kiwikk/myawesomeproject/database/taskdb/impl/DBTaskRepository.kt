package com.kiwikk.myawesomeproject.database.taskdb.impl

import android.content.Context
import android.util.Log
import com.google.firebase.FirebaseApp
import com.google.firebase.database.*
import com.kiwikk.myawesomeproject.callbacks.OnGetDoneTasksCallback
import com.kiwikk.myawesomeproject.callbacks.OnGetNotDoneTasksCallback
import com.kiwikk.myawesomeproject.callbacks.OnLikeCallback
import com.kiwikk.myawesomeproject.database.taskdb.IDBTaskHelper
import com.kiwikk.myawesomeproject.database.taskdb.IDBTaskRepository
import com.kiwikk.myawesomeproject.dataclasses.Task
import com.kiwikk.myawesomeproject.di.named.NamedComputation
import rx.Completable
import rx.Scheduler
import rx.Single
import javax.inject.Inject

class DBTaskRepository @Inject constructor(val dbHelper: IDBTaskHelper,
                                           @NamedComputation val subOnScheduler: Scheduler,
                                           context: Context) : IDBTaskRepository {
    private val readableDB = dbHelper.getReadebleDB()
    private val writableDB = dbHelper.getWritableDB()

    var mFirebaseDatabase: FirebaseDatabase
    var mDatabaseReference: DatabaseReference

    init {
        FirebaseApp.initializeApp(context)
        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mFirebaseDatabase.getReference(TASKS)
    }


    override fun insertTask(task: Task): Single<Long> {
        Log.i("DBtaskRepository", "inserttask $task")

        mDatabaseReference
                .child(task.person_name.split('@')[0])
                .push()
                .setValue(task)


        return Single.fromCallable {
            dbHelper.insertTask(task, writableDB)
        }.subscribeOn(subOnScheduler)
    }

    override fun taskIsDone(task: Task): Completable {
        Log.i("DBTaskRepository", "updateTaskDB $task")

        mDatabaseReference
                .child(task.person_name.split("@")[0])
                .orderByChild("name")
                .equalTo(task.name)
                .addListenerForSingleValueEvent(object: ValueEventListener{
                    override fun onDataChange(snapshot: DataSnapshot) {
                        for(i in snapshot.children ){
                            i.ref.setValue(task)
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                       Log.e("DatabaseError", error.message)
                    }

                })


        return Completable.fromCallable {
            dbHelper.updateTask(task, writableDB)
        }.subscribeOn(subOnScheduler)

    }


    override fun setLikes(task: Task, onLikeCallback: OnLikeCallback) {
        Log.i("DBTaskRepository", "updateTaskDB $task")

        mDatabaseReference
                .child(task.person_name.split("@")[0])
                .orderByChild("name")
                .equalTo(task.name)
                .addListenerForSingleValueEvent(object: ValueEventListener{
                    override fun onDataChange(snapshot: DataSnapshot) {
                        for(i in snapshot.children ){
                            val subtask = i.getValue(Task::class.java)
                            i.ref.removeValue()
                            subtask!!.likes++
                            onLikeCallback.onLike(subtask.likes)
                            i.ref.setValue(subtask)
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.e("DatabaseError", error.message)
                    }

                })
    }

    override fun getDoneTasks(userName: String, onDoneTasks: OnGetDoneTasksCallback) {
        mDatabaseReference
                .child(userName)
                .orderByChild("done")
                .equalTo(true)
                .addListenerForSingleValueEvent(object:ValueEventListener{
                    override fun onDataChange(snapshot: DataSnapshot) {
                        onDoneTasks.getDoneTasks(snapshot.childrenCount)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }

                })
    }

    override fun getNotDoneTasks(userName: String, onNotDoneTasks: OnGetNotDoneTasksCallback) {
        mDatabaseReference
                .child(userName)
                .orderByChild("done")
                .equalTo(false)
                .addListenerForSingleValueEvent(object:ValueEventListener{
                    override fun onDataChange(snapshot: DataSnapshot) {
                        onNotDoneTasks.getNotDoneTasks(snapshot.childrenCount)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }

                })
    }


    override fun closeDB() {
        readableDB.close()
        writableDB.close()
    }

    companion object {
        const val RELATIONS = "relations"
        const val TASKS = "tasks"
        const val USER = "users"
    }

}