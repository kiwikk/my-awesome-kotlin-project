package com.kiwikk.myawesomeproject.database.relation

import com.kiwikk.myawesomeproject.callbacks.OnSearchCallback

interface IRelationDbRep {
    fun subscribe(from: String, to: String)
    fun unsubscribe(from: String, to: String)
    fun checkSubscribe(from: String, to: String, onSearchCallback: OnSearchCallback)
}