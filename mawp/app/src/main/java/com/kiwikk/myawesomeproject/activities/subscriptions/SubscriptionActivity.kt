package com.kiwikk.myawesomeproject.activities.subscriptions

import android.os.Bundle
import com.ismaeldivita.chipnavigation.ChipNavigationBar
import com.kiwikk.myawesomeproject.AndroidApplication
import com.kiwikk.myawesomeproject.R
import com.kiwikk.myawesomeproject.activities.subscriptions.mvp.ISubView
import com.kiwikk.myawesomeproject.activities.subscriptions.mvp.SubPresenter
import com.kiwikk.myawesomeproject.di.components.ActivityComponent
import com.kiwikk.myawesomeproject.fragments.subscription.impl.SubscriberFragment
import com.kiwikk.myawesomeproject.fragments.subscription.impl.SubscriptionsFragment
import com.kiwikk.myawesomeproject.fragments.subscription.impl.SearchFragment
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class SubscriptionActivity : MvpAppCompatActivity(), ISubView {
    lateinit var component: ActivityComponent
        private set

    private var chipNavigationBar: ChipNavigationBar? = null


    lateinit var ionFragment: SubscriptionsFragment
    lateinit var erFragment: SubscriberFragment
    lateinit var searchFragment: SearchFragment


    @InjectPresenter
    lateinit var mPresenter: SubPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        component = (application as AndroidApplication).component.activityComponent()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription)
        supportActionBar?.hide()
        initFragments()

        chipNavigationBar = findViewById(R.id.chipNavigation_subscriptions)
        chipNavigationBar?.setItemSelected(R.id.subscriptions, true)

        chipNavigationBar?.setOnItemSelectedListener(object : ChipNavigationBar.OnItemSelectedListener {
            override fun onItemSelected(id: Int) {
                when (id) {
                    R.id.subscriptions -> mPresenter.beginIonTransaction()
                    R.id.subscribers -> mPresenter.beginErTransaction()
                    R.id.search -> mPresenter.beginSearchTransaction()
                }
            }
        })
    }

    override fun initFragments() {
        ionFragment = SubscriptionsFragment()
        erFragment = SubscriberFragment()
        searchFragment = SearchFragment()
    }

    override fun beginIonTransaction() {
        supportFragmentManager.beginTransaction().replace(R.id.subscriptions_container, ionFragment).commit()
    }

    override fun beginErTransaction() {
        supportFragmentManager.beginTransaction().replace(R.id.subscriptions_container, erFragment).commit()
    }

    override fun beginSearchTransaction() {
        supportFragmentManager.beginTransaction().replace(R.id.subscriptions_container, searchFragment).commit()
    }
}