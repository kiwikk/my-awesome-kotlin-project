package com.kiwikk.myawesomeproject.database.taskdb

import android.database.sqlite.SQLiteDatabase
import com.kiwikk.myawesomeproject.dataclasses.Task

interface IDBTaskHelper {
    fun insertTask(task: Task,
                   db: SQLiteDatabase): Long

    fun updateTask(task: Task,
                   db: SQLiteDatabase
    )

    fun getReadebleDB(): SQLiteDatabase
    fun getWritableDB(): SQLiteDatabase
}