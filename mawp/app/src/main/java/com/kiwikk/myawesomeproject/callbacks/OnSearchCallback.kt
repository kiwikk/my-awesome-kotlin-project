package com.kiwikk.myawesomeproject.callbacks

interface OnSearchCallback {
    fun onResult(res:Boolean)
}