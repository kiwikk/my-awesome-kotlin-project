package com.kiwikk.myawesomeproject.fragments.main.settings.impl

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.android.material.datepicker.*
import com.kiwikk.myawesomeproject.activities.main.MainActivity
import com.kiwikk.myawesomeproject.R
import com.kiwikk.myawesomeproject.activities.subscriptions.SubscriptionActivity
import com.kiwikk.myawesomeproject.callbacks.OnGetDoneTasksCallback
import com.kiwikk.myawesomeproject.callbacks.OnGetNotDoneTasksCallback
import com.kiwikk.myawesomeproject.fragments.main.settings.SettingsView
import com.kiwikk.myawesomeproject.dataclasses.Person
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import java.lang.String.valueOf
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


//данными занимается интерактор (Model в MVP)
/**
 * A simple [Fragment] subclass.
 * Use the [SettingsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SettingsFragment : MvpAppCompatFragment(), SettingsView {
    lateinit var inflater: LayoutInflater
    lateinit var getDonutDialog: AlertDialog
    lateinit var dob: TextView

    @Inject
    @InjectPresenter
    lateinit var sPresenter: SettingsPresenter


    @ProvidePresenter
    fun provide() = sPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        (activity as MainActivity).component.inject(this)

        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        this.inflater = inflater
        val vview = inflater.inflate(R.layout.fragment_settings, container, false)
        setButtons(vview)

        return vview
    }

    private fun setButtons(vview: View) {
        val imageView = vview.findViewById<ImageView>(R.id.profile_image)
        val emailTextView = vview.findViewById<TextView>(R.id.profile_email)
        dob = vview.findViewById(R.id.profile_date_of_birth)

        val taskDoneTextView = vview.findViewById<TextView>(R.id.tv_tasks_done)
        val taskNotDone = vview.findViewById<TextView>(R.id.tv_tasks_not_done)

        val changeBDateButton = vview.findViewById<Button>(R.id.change_bday)
        val getDonut = vview.findViewById<Button>(R.id.getDonut)
        val showSubscribtionsButton = vview.findViewById<Button>(R.id.show_subscribers_btn)

        showSubscribtionsButton.setOnClickListener {
            startActivity(Intent(activity, SubscriptionActivity::class.java))
        }

        val account = (activity as MainActivity).googleSignInAccount
        emailTextView.text = account?.email
        Glide.with(this).load(valueOf(account?.photoUrl)).into(imageView)

        sPresenter.getDoneTasks(account!!.email!!.split("@")[0], object : OnGetDoneTasksCallback {
            override fun getDoneTasks(tasks: Long) {
               taskDoneTextView.text = "Сделано: $tasks"
            }
        })

        sPresenter.getNotDoneTasks(account.email!!.split("@")[0], object : OnGetNotDoneTasksCallback {
            override fun getNotDoneTasks(tasks: Long) {
               taskNotDone.text = "Не сделано: $tasks"
            }

        })


        changeBDateButton.setOnClickListener { sPresenter.setNewBDate() }
        getDonut.setOnClickListener { sPresenter.showDonut() }
        createGetDonutAlert()
    }

    override fun setNewBDate(person: Person) {
        val dataPicker = MaterialDatePicker.Builder
                .datePicker()
                .setCalendarConstraints(limitRange())
                .setTitleText("Ну меняй, меняй")
                .build()

        dataPicker.isCancelable = false
        dataPicker.addOnPositiveButtonClickListener { it ->
            val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
            calendar.timeInMillis = it
            val fm = SimpleDateFormat("dd MM yyyy")
            sPresenter.updateBDate(fm.format(calendar.time))
        }
        dataPicker.show(activity!!.supportFragmentManager, "HomeFragment")
    }

    private fun limitRange(): CalendarConstraints {
        val maxDate = Calendar.getInstance()
        val minDate = Calendar.getInstance()
        minDate.add(Calendar.YEAR, -80)
        val dateValidatorMin: CalendarConstraints.DateValidator = DateValidatorPointForward.from(minDate.timeInMillis)
        val dateValidatorMax: CalendarConstraints.DateValidator = DateValidatorPointBackward.before(maxDate.timeInMillis)

        val constraintsBuilder = CalendarConstraints.Builder()

        val listValidators = ArrayList<CalendarConstraints.DateValidator>()
        listValidators.add(dateValidatorMin)
        listValidators.add(dateValidatorMax)
        val validators = CompositeDateValidator.allOf(listValidators)
        constraintsBuilder.setValidator(validators)

        return constraintsBuilder.build()
    }


    private fun createGetDonutAlert() {
        val builder = AlertDialog.Builder(context)
        val dInf = inflater.inflate(R.layout.donut_layout, null)

        builder.setView(dInf)
        builder.setTitle("Держи :з")

        builder.setPositiveButton("Спасибо") { _, _ ->
            Toast.makeText(context,
                    "Обращайся))", Toast.LENGTH_LONG).show()
        }

        builder.setNegativeButton("Нет ") { _, _ ->
            Toast.makeText(context,
                    "Ну нет так нет", Toast.LENGTH_LONG).show()
        }

        getDonutDialog = builder.create()
        getDonutDialog.setOnDismissListener { sPresenter.hideDonut() }
    }

    override fun showDonut() {
        getDonutDialog.show()
    }

    override fun hideDonnut() {
        getDonutDialog.dismiss()
    }

    override fun setBDate(bDate: String) {
        dob.text = bDate.replace(' ', '.')
    }

    override fun showUpdateResult(text: String) {

        Toast.makeText(context, text, Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()

        getDonutDialog.setOnDismissListener(null)
        getDonutDialog.dismiss()
    }


    companion object {
        private const val ARG_PARAM1: String = "param1"
        private const val ARG_PARAM2: String = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SettingsFragment.
         */
        fun newInstance(param1: String?, param2: String?): SettingsFragment {
            val fragment = SettingsFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}