package com.kiwikk.myawesomeproject.di.modules

import com.kiwikk.myawesomeproject.di.scopes.MainActivityScope
import com.kiwikk.myawesomeproject.fragments.main.settings.ISettingsInteractor
import com.kiwikk.myawesomeproject.fragments.main.settings.SettingsView
import com.kiwikk.myawesomeproject.fragments.main.settings.impl.SettingsInteractor
import com.kiwikk.myawesomeproject.fragments.main.settings.impl.SettingsPresenter
import dagger.Binds
import dagger.Module
import moxy.MvpPresenter

@Module
interface SettingsModule {
    @Binds
    @MainActivityScope
    fun provideSettingsInteractor(sInteractor: SettingsInteractor): ISettingsInteractor

    @Binds
    @MainActivityScope
    fun providePresenter(sPresenter: SettingsPresenter): MvpPresenter<SettingsView>
}