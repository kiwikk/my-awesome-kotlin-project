package com.kiwikk.myawesomeproject.di.modules

import com.kiwikk.myawesomeproject.database.relation.IRelationDbRep
import com.kiwikk.myawesomeproject.database.relation.impl.RelationDBRep
import com.kiwikk.myawesomeproject.di.scopes.MainActivityScope
import com.kiwikk.myawesomeproject.fragments.subscription.ISubscriptionInteractor
import com.kiwikk.myawesomeproject.fragments.subscription.impl.SubscriptionInteractor
import dagger.Binds
import dagger.Module

@Module
interface SearchModule {
    @Binds
    @MainActivityScope
    fun provideSearchInteractor(interactor: SubscriptionInteractor): ISubscriptionInteractor

    @Binds
    @MainActivityScope
    fun provideRelation(dbRep: RelationDBRep): IRelationDbRep
}