package com.kiwikk.myawesomeproject.dataclasses

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

data class Person(var _id: Int = -1,
                  var name: String = "",
                  var birthDate: String = LocalDate.now().format(DateTimeFormatter.ofPattern("dd MM yyyy")),
                  var imageUrl: String = "",
                  var weeks:Int = 0
)