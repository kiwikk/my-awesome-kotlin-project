package com.kiwikk.myawesomeproject.database.taskdb.impl

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.os.Looper
import android.util.Log
import com.kiwikk.myawesomeproject.database.taskdb.IDBTaskHelper
import com.kiwikk.myawesomeproject.dataclasses.Task
import javax.inject.Inject

class DataBaseTaskHelper @Inject constructor(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION), IDBTaskHelper {

    override fun onCreate(db: SQLiteDatabase?) {
        db!!.execSQL("CREATE TABLE $DB_NAME ("
                + "$ID INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "$PERSON_ID TEXT, "
                + "$WEEK_ID INTEGER, "
                + "$NAME TEXT, "
                + "$DESCRIPTION TEXT, "
                + "$TYPE TEXT, "
                + "$IS_DONE TEXT, "
                + "$DATE TEXT, "
                + "$PRIVATE TEXT);")
    }

    override fun insertTask(task: Task,
                            db: SQLiteDatabase
    ): Long {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw RuntimeException()
        }

        val contentValues = getContentValues(task)

        Log.d("Colums:", contentValues.valueSet().toString())

        return db.insert(DB_NAME, null, contentValues)
    }

    override fun updateTask(task: Task, db: SQLiteDatabase) {
        val contentValues = getContentValues(task)

        Log.d("id", task.toString())
        db.update(DataBaseTaskHelper.DB_NAME,
                contentValues,
                "_id=${task.id}", null)
    }

    private fun getContentValues(task: Task): ContentValues {
        val contentValues = ContentValues()

        contentValues.put(PERSON_ID, task.person_name)
        contentValues.put(WEEK_ID, task.week_id)
        contentValues.put(NAME, task.name)
        contentValues.put(DESCRIPTION, task.description)
        contentValues.put(TYPE, task.type)
        contentValues.put(PRIVATE, task.isPrivate.toString())
        contentValues.put(IS_DONE, task.isDone.toString())
        contentValues.put(DATE, task.date)

        return contentValues
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        TODO("Not yet implemented")
    }

    override fun getReadebleDB(): SQLiteDatabase = readableDatabase
    override fun getWritableDB(): SQLiteDatabase = writableDatabase

    companion object {
        private const val DB_NAME: String = "Tasks"
        private const val DB_VERSION = 1
        const val ID = "_id"
        const val PERSON_ID = "PERSON_ID"
        const val NAME = "NAME"
        const val DESCRIPTION = "DESCRIPTION"
        const val PRIVATE = "PRIVATE"
        const val TYPE = "TYPE"
        const val WEEK_ID = "WEEK_ID"
        const val IS_DONE = "IS_DONE"
        const val DATE = "DATE"
    }
}