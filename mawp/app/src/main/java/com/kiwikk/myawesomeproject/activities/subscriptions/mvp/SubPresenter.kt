package com.kiwikk.myawesomeproject.activities.subscriptions.mvp

import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
class SubPresenter : MvpPresenter<ISubView>() {
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.initFragments()
        beginIonTransaction()
    }

    fun beginIonTransaction() {
        viewState.beginIonTransaction()
    }

    fun beginErTransaction() {
        viewState.beginErTransaction()
    }

    fun beginSearchTransaction() {
        viewState.beginSearchTransaction()
    }
}